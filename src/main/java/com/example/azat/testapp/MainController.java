package com.example.azat.testapp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

  @RequestMapping(value = "/calculateislands", method = RequestMethod.GET, params = {"x","y"})
  public String getIslandsCount(Model model, @RequestParam(value = "x", required = true) int x, @RequestParam(value = "y", required = true) int y){
    CalculateIslands calculateIslands = new CalculateIslands(x, y);
    calculateIslands.generateMatrix();
    char[][] massive = new char[x][y];
    for (int i = 0; i < x; i++){
      for (int j = 0; j < y; j++){
        massive[i][j] = calculateIslands.mas[i][j] == 0 ? 'O' : 'X';
      }
    }
    model.addAttribute("massive", massive);
    calculateIslands.calculateIslands();
    model.addAttribute("count", calculateIslands.count);
    return "calculateIslandsCount";
  }
}
