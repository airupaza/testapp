package com.example.azat.testapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalculateIslands {

    Logger logger = LoggerFactory.getLogger(CalculateIslands.class);

    int[][] mas;
    int count;
    int x;
    int y;

  public CalculateIslands(int x, int y) {
    this.x = x;
    this.y = y;
    count = 0;
    mas = new int[x][y];
  }

    public void checkNeighboringCells(int i, int j){
      try{
        if (mas[i][j] == 0) return;
        logger.info("Check cell [" + i + "][" + j + "]");
        mas[i][j] = 0;
        checkNeighboringCells(i, j+1);
        checkNeighboringCells(i+1, j+1);
        checkNeighboringCells(i+1, j+1);
        checkNeighboringCells(i+1, j);
        checkNeighboringCells(i+1, j-1);
        checkNeighboringCells(i, j-1);
        checkNeighboringCells(i-1, j-1);
        checkNeighboringCells(i-1, j);
        checkNeighboringCells(i-1, j+1);
      }catch (ArrayIndexOutOfBoundsException e){
          logger.debug("Catching ArrayIndexOutOfBoundsException at [" + i + "][" + j + "], that's all right");
      }

    }

  public void generateMatrix(){
    for (int i = 0; i < x; i++){
      for (int j = 0; j < y; j++){
        mas[i][j] = (int) Math.round( Math.random() );
      }
    }

    for (int i = 0; i < x; i++){
      for (int j = 0; j < y; j++){
        System.out.print(mas[i][j] == 0 ? "O " : "X " );
      }
      System.out.println();
    }
  }

  public void calculateIslands(){
    for (int i = 0; i < x; i++){
      for (int j = 0; j < y; j++){
        if (mas[i][j] == 1){
          count++;
          checkNeighboringCells(i, j);
        }
      }
    }
  }


}
